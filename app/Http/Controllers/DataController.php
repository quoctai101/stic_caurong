<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Views
    public function getLatestRecord()
    {
        $sensorInfos = DB::table('cr_sensor')
                        ->get();
		$sensorValues = [];
		$sensorValues['status'] = [];
		foreach($sensorInfos as $sensorInfo)
		{
			$value = DB::table('cr_recordLine')->select('Value')
						->where('SensorID', '=', $sensorInfo->ID)
						->take(1)->orderBy('DateTime', 'DESC')
						->get();
			if(isset($value[0]->Value)) $sensorValues[$sensorInfo->Sign] = $value[0]->Value;
			if($value[0]->Value > $sensorInfo->UpLimit || $value[0]->Value < $sensorInfo->DownLimit)
				$sensorValues['status'][$sensorInfo->Sign] = 0;
			else $sensorValues['status'][$sensorInfo->Sign] = 1;
		}
        return view('bangBieu', ['sensorInfos' => $sensorInfos, 'sensorValues' => $sensorValues]);
    }
    public function getTongQuan($slug = 'ung-suat-nhom-1')
    {
        $typeUni = array(
            'ung-suat-nhom-1' => array(
                'name' => "Ứng suất (nhóm 1)",
                'query' => "Ung suat nhom 1",
                'col' => '4'
            ),
            'ung-suat-nhom-2' => array(
                'name' => "Ứng suất (nhóm 2)",
                'query' => "Ung suat nhom 2",
                'col' => '6'
            ),
            'ung-suat-cuc-bo' => array(
                'name' => "Ứng suất cục bộ",
                'query' => "Ung suat cuc bo",
                'col' => '6'
            ),
            'chuyen-vi' => array(
                'name' => "Chuyển vị",
                'query' => "Chuyen vi",
                'col' => '12'
            ),
            'goc-xoay' => array(
                'name' => "Góc xoay",
                'query' => "Goc xoay",
                'col' => '12'
            ),
            'nhiet-do' => array(
                'name' => "Nhiệt độ",
                'query' => "Nhiet do",
                'col' => '4'
            ),
			'gia-toc' => array(
                'name' => "Gia tốc",
                'query' => "Gia toc",
                'col' => '6'
            )
        );
        if(!isset($typeUni[$slug])) return redirect('loi');
        $type = $typeUni[$slug];
        $type['section'] = [];
        $sections = DB::table('cr_sensor')
                        ->distinct()->select('Section')
                        ->where('Section', "LIKE", $type['query'] . "%")
                        ->get();
        foreach($sections as $section)
        {
            $type['section'][] = $section->Section;
        }
        return view('tongQuan.tongQuan', ['type' => $type]);
    }
    public function lichSuDuLieu($sign = '')
    {
        $sensorData = [];
        if($sign) $sensorData = $this->getSensorData($sign);
        $sensors = DB::table('cr_sensor')
                        ->select('Sign')
                        ->get();
        return view('lichSuDulieu', ['sensors' => $sensors, 'sensorData' => $sensorData]);
    }
    // Utilities
    public function getSensorData($sign, $fromDate = '' , $toDate = '')
    {
        $result = [];
        if(!$fromDate)
            $db = DB::table('cr_recordLine')
                    ->leftJoin('cr_sensor', 'cr_recordLine.SensorID', '=', 'cr_sensor.ID')
                    ->select('cr_recordLine.Value', 'cr_recordLine.DateTime')
                    ->where('cr_sensor.Sign', $sign)
                    ->take(100)
                    ->orderBy('DateTime', 'DESC');
        else
            $db = DB::table('cr_recordLine')
                    ->leftJoin('cr_sensor', 'cr_recordLine.SensorID', '=', 'cr_sensor.ID')
                    ->select('cr_recordLine.Value', 'cr_recordLine.DateTime')
                    ->where('cr_sensor.Sign', $sign)
                    ->whereBetween('cr_recordLine.DateTime', [$fromDate, $toDate])
                    ->orderBy('DateTime', 'DESC');
        if($db->count())
        {
            $rows = $db->get();
            $result['sign'] = $sign;
            $result['data'] = [];
            $result['labels'] = [];
            foreach($rows as $row)
            {
                $result['data'][] = $row->Value;
                $result['labels'][] = $row->DateTime;
            }
			$result['data'] = array_reverse($result['data']);
			$result['labels'] = array_reverse($result['labels']);
        }
        return $result;
    }
    public function getSectionData($section)
    {
        $result = [];
		$sensorValues = [];
        $db = DB::table('cr_sensor')
                ->where("Section", '=', $section);
        if($db->count())
        {
			$result['title'] = $section;
            $result['data'] = $db->get();
			foreach($result['data'] as $sensorInfo)
			{
				$value = DB::table('cr_recordLine')->select('Value')
							->where('SensorID', '=', $sensorInfo->ID)
							->take(1)->orderBy('DateTime', 'DESC')
							->get();
				if(isset($value[0]->Value)) $sensorValues[$sensorInfo->Sign] = $value[0]->Value;
			}
			$result['sensorValues'] = $sensorValues;
        }
        return $result;
    }
    // API
    public function ajaxNewData(Request $request)
    {
        if($request->input('sign'))
        {
            $sign = $request->input('sign');
            $fromDate = $request->input('fromDate');
            $toDate = $request->input('toDate');
            $sensorData = $this->getSensorData($sign, $fromDate, $toDate);
            return response()->json([
                'sign' => $sensorData['sign'],
                'data' => $sensorData['data'],
                'labels' => $sensorData['labels']
            ]);
        }
    }
    public function ajaxSectionData(Request $request)
    {
        if($request->input('section'))
        {
            $sectionData = $this->getSectionData($request->input('section'));
            return view('tongQuan.detailModal', ['sectionData' => $sectionData]);
        }
    }
}
