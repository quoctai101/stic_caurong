@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Hình ảnh tổng quan</h4>
                </div>
                <div class="card-body text-center">
                    <img src="{{asset('img/cau-rong.jpg')}}" class="img-fluid image-zoom" alt="Cầu Rồng">
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Google Maps</h4>
                </div>
                <div class="card-body text-center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.0634557799694!2d108.22744563122862!3d16.06219659259002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31421961666eff35%3A0xdba86f2bcebf5afc!2zQ-G6p3UgUuG7k25n!5e0!3m2!1svi!2s!4v1592041377742!5m2!1svi!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Map 4D</h4>
                </div>
                <div class="card-body">
                    <iframe src="https://map.map4d.vn/@16.061471,108.227261,19.00,58.8,51.1,1?search=C%E1%BA%A7u+R%E1%BB%93ng+%C4%90%C3%A0+N%E1%BA%B5ng&type=detail&data=5c9845e5814a9df9d67e5bb7" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection