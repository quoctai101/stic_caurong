<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- Head -->
    @include('partials.head')
    <body class="">
        <div class="wrapper">
            <!-- Sidebar -->
            @include('partials.sidebar')
            <div class="main-panel">
                <!-- Navbar -->
                @include('partials.navbar')
                <div class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <!-- Footer -->
                @include('partials.footer')
            </div>
        </div>
        <!--   Core JS Files   -->
        @include('partials.loadjs')
    </body>
</html>