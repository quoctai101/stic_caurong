<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/loi', function () {
    return view('loi');
});

Auth::routes();

Route::get('/tong-quan/{type}', 'DataController@getTongQuan');

Route::get('/bang-bieu', 'DataController@getLatestRecord');

Route::get('/lich-su-du-lieu/{sign?}', 'DataController@lichSuDuLieu');

Route::post('/ajaxNewData', 'DataController@ajaxNewData');

Route::post('/ajaxSectionData', 'DataController@ajaxSectionData');

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
});